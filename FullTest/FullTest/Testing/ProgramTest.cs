﻿using System;
using System.IO;
using System.Windows.Forms;

namespace FullTest
{
    static class ProgramTest
    {
        //Test --> Testing
        //Program --> ProgramTest
        //TODO: маленький баг в chooseyourtest (в edittest пашет)
        /// <summary>
        /// переменнная, разрешаюшая следующую вторую форму
        /// </summary>
        static public bool Open;
        /// <summary>
        /// переменная, разрешающая закрыть окно результата
        /// </summary>
        public static bool Close;
        /// <summary>
        /// имя файл, передающееся во вторую форму
        /// </summary>
        static public string Name;
        /// <summary>
        /// число баллов
        /// </summary>
        public static int Points;
        /// <summary>
        /// перемнная, хранящая путь к папке "мои документы"
        /// </summary>
        private static readonly string Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\HistoryNovikov\";

        /// <summary>
        /// Его величество Main()
        /// </summary>
        /// <returns></returns>
        [STAThread]
        static public int Master()
        {
            Application.EnableVisualStyles();
            //
            try
            {
                var stream1 = new StreamWriter(@Path + "open.tst");
                stream1.Close();
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Ошибка поиска файла. Убедитесь, что программа установлена правильно.\nПопробуйте провести повторную установку.\nВ крайнем случае обратитесь к Егору");
                return (1);
            }
            //первая форма
            Application.Run(new ChooseYourTest());
            if (!Open) return 0;
            Open = !Open;
            //вторая форма
            try
            {
                if (Name != "последний")
                {
                    var stream = new StreamWriter(@Path + "open.tst");
                    stream.Write(Path + Name + ".xml");
                    stream.Close();
                    var stream1 = new StreamWriter(@Path + "last.tst");
                    stream1.Write(Path + Name + ".xml");
                    stream1.Close();
                    Application.Run(new Testing(@Path + "open.tst"));
                }
                else
                    using (var stream = new StreamReader(@Path + "last.tst"))
                        if (stream.ReadLine() != null)
                        {
                            Application.Run(new Testing(@Path + "last.tst"));
                        }
                        else
                        {
                            MessageBox.Show("Извините, такого файла не существует");
                        }
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Ошибка поиска файла. Убедитесь, что программа установлена правильно.\nПопробуйте провести повторную установку.\nВ крайнем случае обратитесь к Егору");
                return (1);
            }
            //третья форма
            if (!Open) return 0;
            while (!Close)
                Application.Run(new Result(Points));
            return 0;
        }
    }
}
