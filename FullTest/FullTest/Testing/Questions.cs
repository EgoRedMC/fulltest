﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FullTest.Test
{
    class InputQuestion : GroupBox
    {
        #region fields
        /// <summary>
        /// номер вопроса
        /// </summary>
        private int _questionNumber;
        /// <summary>
        /// вопрос
        /// </summary>
        private Label _questionText;
        /// <summary>
        /// лейбл с тесктом совета
        /// </summary>
        private Label _help;
        /// <summary>
        /// текстбокс для ответа
        /// </summary>
        private TextBox _answer;
        /// <summary>
        /// правильный ответ
        /// </summary>
        private string _correctAnswer;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        /// <param name="number">номер вопроса</param>
        public InputQuestion(int number)
        {
            FieldDeclaration(number);
            Size = new Size(400, 70);
            Location = new Point(10, 40 + (number + 1) * 200);
            AddToControls();
        }
        /// <summary>
        /// конструктор для считывания из xml файла
        /// </summary>
        /// <param name="number">номер вопроса</param>
        /// <param name="question">заготовка в формате InputQuestionInfo</param>
        public InputQuestion(int number, InputQuestionInfo question)
        {
            //форма
            _questionNumber = number;
            Size = new Size(540, 70);
            Location = new Point(10, 40 + Testing.Ssize.Sum() + Testing.Csize.Sum()+Testing.Wsize.Sum());
            Testing.Wsize.Add(70);
            //вопрос
            _questionText = new Label() { Text = question.Text };
            //правильный ответ
            _correctAnswer = question.Answer;
            //задание размеров и положений
            LocaleElements();
            AddToControls();
        }
        /// <summary>
        /// функция, задающая элементам положения и размеры
        /// </summary>
        private void LocaleElements()
        {
            //лейбл _help
            _help = new Label()
            {
                Size = new Size(450, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 30),
                Text = "Ответ записывайте с большой буквы"
            };
            //вопрос
            _questionText.Size = new Size(450, 20);
            _questionText.BorderStyle = BorderStyle.FixedSingle;
            _questionText.Location = new Point(10, 10);
            //текстбокс для ответа
            _answer = new TextBox()
            {
                Size = new Size(450, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 50)
            };
        }
        /// <summary>
        /// функция, добавляющая элементы на форму
        /// </summary>
        private void AddToControls()
        {
            Controls.Add(_questionText);
            Controls.Add(_help);
            Controls.Add(_answer);
        }
        /// <summary>
        /// функция по умолчанию, задающая элементам параметры
        /// </summary>
        /// <param name="number">номер вопроса</param>
        private void FieldDeclaration(int number)
        {
            _questionNumber = number + 2;
            //разбираемся с лабелом
            _questionText = new Label
            {
                Size = new Size(350, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 10),
                Text = "Вопрос " + (_questionNumber)
            };
            //с лабелом Help ( в нем подсказки тип)
            _help = new Label
            {
                Size = new Size(350, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 30),
                Text = "Ответ записывайте с большой буквы"
            };
            //с текстбоксом
            _answer = new TextBox
            {
                Size = new Size(350, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 50),
                Text = ""
            };
            //
            _correctAnswer = "1";
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// функция проверки
        /// </summary>
        /// <returns>1 - верно, 0 - ложно</returns>
        public int Check()
        {
            int point = 0;
            if (_answer.Text == _correctAnswer)
                point++;
            return point;
        }
        /// <summary>
        /// функция, вписывающая в текстбокс правильный ответ
        /// </summary>
        public void Cheat()
        {
            _answer.Text = _correctAnswer;
        }
        #endregion
    }

    class QuestionCheck : GroupBox
    {
        #region fields
        /// <summary>
        /// рандом (на всякий случай)
        /// </summary>
        private static readonly Random R = new Random();
        /// <summary>
        /// массив ответов
        /// </summary>
        private List<CheckBox> _answers;
        /// <summary>
        /// вопрос
        /// </summary>
        private Label _questionText;
        /// <summary>
        /// массив правильных ответов
        /// </summary>
        private List<int> _correctAnswers;
        /// <summary>
        /// номер ответа
        /// </summary>
        private int _questionNumber;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        /// <param name="number">номер вопроса</param>
        public QuestionCheck(int number)
        {
            FieldDeclaration(4, number);
            Size = new Size(400, 200);
            Location = new Point(10, 40 + (number) * Size.Height);
            Randomize();
            AddToControls();
        }
        /// <summary>
        /// конструктор для считывания из xml файла
        /// </summary>
        /// <param name="number">номер вопроса</param>
        /// <param name="question">xml заготовка, формата QuestionCheckInfo</param>
        public QuestionCheck(int number, QuestionCheckInfo question)
        {
            //ответы
            _answers = new List<CheckBox>();
            for (var i = 0; i < question.Answers.Count; i++)
                _answers.Add(new CheckBox() { Text = question.Answers[i] });
            //вопрос
            _questionText = new Label() { Text = question.Text };
            //номер вопроса
            _questionNumber = number;
            //номера ответов
            _correctAnswers = new List<int>(question.Rightanswers.Count);
            for (var i = 0; i < _correctAnswers.Capacity; i++)
                _correctAnswers.Add(question.Rightanswers[i]);
            //возможно поменять местами
            Location = new Point(10, 40 + Testing.Ssize.Sum() + Testing.Csize.Sum());
            //форма
            Testing.Csize.Add( 10 + 20 + 10 + 30 * _answers.Count);
            Size = new Size(540, Testing.Csize[number-1]);
            //задача элементам положений и размеров
            LocaleElements();
            Randomize();
            AddToControls();
        }
        /// <summary>
        /// функция, задающая положения и размеры элементов
        /// </summary>
        private void LocaleElements()
        {
            //ответы
            for (int i = 0; i < _answers.Count; i++)
            {
                _answers[i].Size = new Size(450, 20);
                _answers[i].Location = new Point(10, 10 + 20 + 10 + 30 * i);
            }
            //вопрос
            _questionText.Size = new Size(450, 20);
            _questionText.BorderStyle = BorderStyle.FixedSingle;
            _questionText.Location = new Point(10, 10);

        }
        /// <summary>
        /// функция, добавлющая элементы на форму
        /// </summary>
        private void AddToControls()
        {
            Controls.Add(_questionText);
            foreach (var a in _answers)
            {
                Controls.Add(a);
            }
        }
        /// <summary>
        /// функция, задающая элементам параметры по умолчанию
        /// </summary>
        /// <param name="variants">число вариантов ответов</param>
        /// <param name="number">номер ответа</param>
        private void FieldDeclaration(int variants, int number)
        {
            _questionNumber = number + 1;
            _questionText = new Label
            {
                Size = new Size(350, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 10),
                Text = "Вопрос " + (_questionNumber)
            };
            _answers = new List<CheckBox>(variants);
            _correctAnswers = new List<int>(2) { 0, 3 };
            for (var i = 0; i < variants; i++)
            {
                _answers.Add(new CheckBox
                {
                    Text = "Ответ № " + (i + 1),
                    Size = new Size(350, 20),
                    Location = new Point(10, 10 + 20 + 10 + 30 * i)
                });
            }
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// функция проверки
        /// </summary>
        /// <returns>1 - правильно, 0 - ошибочно</returns>
        public int Check()
        {
            var points = 0;
            if ((CountChecked() == _correctAnswers.Count) & (RightChecked()))
                points++;
            return points;
        }
        /// <summary>
        /// вспомогательная функция для проверки
        /// </summary>
        /// <returns>число отмеченных ответов</returns>
        private int CountChecked()
        {
            int temp = 0;
            foreach (var a in _answers)
                if (a.Checked)
                    temp++;
            return temp;
        }
        /// <summary>
        /// вспомогательная функция для проверки
        /// </summary>
        /// <returns>все ли правильные отмечены</returns>
        private bool RightChecked()
        {
            bool temp = true;
            foreach (var c in _correctAnswers)
                if (!_answers[c].Checked)
                    temp = false;
            return temp;
        }
        /// <summary>
        /// функция, отмечающая правильные и убирая отметки с неправильных
        /// </summary>
        public void Cheat()
        {
            foreach (CheckBox a in _answers)
                a.Checked = false;
            foreach (int ca in _correctAnswers)
                _answers[ca].Checked = true;
        }
        /// <summary>
        /// функция, расставляющая ответы в случайном порядке
        /// </summary>
        private void Randomize()
        {
            for (var i = 0; i < _answers.Count; i++)
            {
                var randomed = R.Next(_answers.Count);
                if (randomed == i) continue;
                var temp = _answers[i].Location;
                _answers[i].Location = _answers[randomed].Location;
                _answers[randomed].Location = temp;
            }
        }
        #endregion
    }

    class SelectQuestion : GroupBox
    {
        #region fields
        /// <summary>
        /// рандом (на всякий случай)
        /// </summary>
        private static readonly Random R = new Random();
        /// <summary>
        /// номер вопроса
        /// </summary>
        private int _questionNumber;
        /// <summary>
        /// номер правильного ответа
        /// </summary>
        private int _correctAnswer;
        /// <summary>
        /// вопрос
        /// </summary>
        private Label _questionText;
        /// <summary>
        /// массив ответов
        /// </summary>
        private List<RadioButton> _answers;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        /// <param name="number">номер вопроса в тесте</param>
        public SelectQuestion(int number)
        {
            FieldDeclaration(4, number);
            Randomize();
            AddToControls();
        }
        /// <summary>
        /// конструктор для считывания из xml файла
        /// </summary>
        /// <param name="number">номер вопроса</param>
        /// <param name="question">xml файл типа SelectQuestionInfo</param>
        public SelectQuestion(int number, SelectQuestionInfo question)
        {
            //ответы
            _answers = new List<RadioButton>(question.Answers.Count);
            for (int i = 0; i < _answers.Capacity; i++)
                _answers.Add(new RadioButton()
                {
                    Text = question.Answers[i],
                    Location = new Point(10, 10 + 20 + 10 + 30 * i),
                    Size = new Size(450, 20)
                }
                    );
            //номер вопроса
            _questionNumber = number;
            //номер правильного ответа
            _correctAnswer = question.CorrectAnswer;
            //вопрос
            _questionText = new Label() { Text = question.Text };
            //форма
            Testing.Ssize.Add(160);
            Size = new Size(540, Testing.Ssize[_questionNumber - 1]);
            Location = new Point(10, 40 + (number - 1) * Size.Height);
            //установка размеров и положений элементов
            LocaleElements();
            Randomize();
            AddToControls();
        }
        /// <summary>
        /// функция, задающая элементам местоположения и размеры
        /// </summary>
        private void LocaleElements()
        {
            //ответы
            for (int i = 0; i < _answers.Count; i++)
            {
                _answers[i].Location = new Point(10, 10 + 20 + 10 + 30 * i);
                _answers[i].Size = new Size(450, 20);
            }
            //вопрос
            _questionText.Size = new Size(450, 20);
            _questionText.Location = new Point(10, 10);
            _questionText.BorderStyle = BorderStyle.FixedSingle;
        }
        /// <summary>
        /// функция, устанавливающая элементы на форму
        /// </summary>
        private void AddToControls()
        {
            Controls.Add(_questionText);
            foreach (var a in _answers)
                Controls.Add(a);
        }
        /// <summary>
        /// функция по умолчанию, задающая параметры всем элементам на форме
        /// </summary>
        /// <param name="variants">число вариантов ответов</param>
        /// <param name="number">номер вопроса</param>
        private void FieldDeclaration(int variants, int number)
        {
            _questionNumber = number + 1;
            _questionText = new Label
            {
                Size = new Size(350, 20),
                BorderStyle = BorderStyle.FixedSingle,
                Location = new Point(10, 10),
                Text = "Вопрос " + (_questionNumber.ToString())
            };
            _answers = new List<RadioButton>();
            _correctAnswer = 2;
            //ошбика, использовать Add();
            for (var i = 0; i < variants; i++)
            {
                _answers.Add(new RadioButton
                {
                    Text = String.Format("Ответ № {0}", i + 1),
                    Size = new Size(350, 20),
                    Location = new Point(10, 10 + 20 + 10 + 30 * i)
                });
            }
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// функция, проверяющая, правилен ли ответ на вопрос
        /// </summary>
        /// <returns>правильность</returns>
        public int Check()
        {
            return _answers[_correctAnswer].Checked ? 1 : 0;
        }
        /// <summary>
        /// функция, отмечающая правильный ответ
        /// </summary>
        public void Cheat()
        {
            _answers[_correctAnswer].Checked = true;
        }
        /// <summary>
        /// функция, размещающая элементы в случайном положении
        /// </summary>
        private void Randomize()
        {
            for (var i = 0; i < _answers.Count; i++)
            {
                var randomed = R.Next(_answers.Count);
                if (randomed == i) continue;
                var temp = _answers[i].Location;
                _answers[i].Location = _answers[randomed].Location;
                _answers[randomed].Location = temp;
            }
        }
        #endregion
    }
}
