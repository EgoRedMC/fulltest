﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FullTest
{
    class Result : Form
    {
        #region fields
        /// <summary>
        /// текстбокс для пароля
        /// </summary>
        private readonly TextBox _pass;
        /// <summary>
        /// сам пароль
        /// </summary>
        private const string Password = "1921";
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public Result(int points)
        {
            //форма
            ControlBox = false;
            MaximizeBox = false;
            Size = new Size(300, 200);
            Text = "Результат";
            FormBorderStyle = FormBorderStyle.FixedSingle;
            //текст
            var labelPoints = new Label()
            {
                Text = String.Format("У вас {0} правильных ответов", points),
                Size = new Size(200, 20),
                Location = new Point(65, 30)
            };
            Controls.Add(labelPoints);
            //текстбокс
            _pass = new TextBox()
            {
                PasswordChar = '*',
                Size = new Size(150, 20),
                Location = new Point(70, 60)
            };
            Controls.Add(_pass);
            //кнопка
            var ok = new Button()
            {
                Text = "Ок",
                Size = new Size(50, 20),
                Location = new Point(120, 90)
            };
            Controls.Add(ok);
            ok.Click += OkClick;
            _pass.KeyPress += PressEnter;
        }
        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// обработчик события для кнопки ОК
        /// если пароль в текстбоксе равен заданному, то форма закрывается
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkClick(object sender, EventArgs e)
        {
            if (_pass.Text == Password)
            {
                ProgramTest.Close = true;
                Close();
            }
        }
        /// <summary>
        /// обработчик события, реагирующий на нажатие клавиши Еnter на тектбоксе
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PressEnter(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                if (_pass.Text == Password)
                {
                    ProgramTest.Close = true;
                    Close();
                }
        }
        #endregion
    }
}
