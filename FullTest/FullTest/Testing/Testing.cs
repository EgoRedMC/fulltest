﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using FullTest.Test;

namespace FullTest
{
    class Testing : Form
    {
        #region fields
        /// <summary>
        /// рандом
        /// </summary>
        private static readonly Random R = new Random();
        /// <summary>
        /// массив Select вопросов
        /// </summary>
        private List<SelectQuestion> _questions;
        /// <summary>
        /// массив check вопросов
        /// </summary>
        private List<QuestionCheck> _questionsC;
        /// <summary>
        /// массив Input вопросов
        /// </summary>
        private List<InputQuestion> _questionsW;
        /// <summary>
        /// кнопка ОК (принятие, проверка)
        /// </summary>
        private Button _ok;
        /// <summary>
        /// Заголовок теста
        /// </summary>
        private Label _head;
        /// <summary>
        /// кол-во баллов
        /// </summary>
        private int _points;
        #region sizes
        /// <summary>
        /// лист размеров SelectQuestion по OY
        /// </summary>
        public static List<int> Ssize;
        /// <summary>
        /// лист размеров QuestionCheck по OY
        /// </summary>
        public static List<int> Csize;
        /// <summary>
        /// лист размеров QuestionCheck по OY
        /// </summary>
        public static List<int> Wsize;
        #endregion
        #region for cheating
        /// <summary>
        /// код, при введении которого забиваются ответы
        ///_pass[0] = "hesoyam";
        ///_pass[1] = "aezakmi";
        ///_pass[2] = "wanrltv";
        /// </summary>
        private readonly string[] _pass;
        /// <summary>
        /// счётчик букв
        /// </summary>
        private int _lettercount;
        /// <summary>
        /// выбранный пароль
        /// </summary>
        private int _passcount;
        /// <summary>
        /// защита от повторного ввода пароля
        /// </summary>
        private int _wrong;
        /// <summary>
        /// защита от повторного ввода пароля
        /// </summary>
        private bool _cheated;
        /// <summary>
        /// защита от повторного ввода пароля
        /// </summary>
        private List<int> _wList;
        #endregion
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public Testing()
        {
            Text = "История";
            AutoScroll = true;
            Size = new Size(445, 600);
            InitComponents(8);
            AddToControls();
        }
        /// <summary>
        /// конструктор формы для считывания из xml файла
        /// </summary>
        /// <param name="fileName">имя файла, который перенаправит на xml файл</param>
        public Testing(string fileName)
        {
            try
            {
                //перенаправление на основной файл
                var stream = new StreamReader(fileName);
                var file = stream.ReadLine();
                stream.Close();
                //работа с формой
                Size = new Size(585, 640);
                HScroll = false;
                AcceptButton = _ok;
                FormBorderStyle = FormBorderStyle.FixedSingle;
                MaximizeBox = false;
                //задание элементам параметров
                ReadFromFile(file);
                AddToControls();
                KeyPreview = true;
                KeyPress += Cheat;
                Shown += Scrolling;
                //переменные для Cheat;
                _pass = new string[3];
                _pass[0] = "hesoyam";
                _pass[1] = "aezakmi";
                _pass[2] = "wanrltv";
            }
            //в случае отсутствия файла
            catch (FileNotFoundException)
            {
                MessageBox.Show("Ошибка загрузки теста из файла. Приложение будет завершено. Убедитесь, что файл данного теста существует.\nВ крайнем случае обратитесь к Егору");
                Environment.Exit(1);
            }
        }
        /// <summary>
        /// добавление элементов на форму
        /// </summary>
        private void AddToControls()
        {
            foreach (var q in _questions)
                Controls.Add(q);
            Controls.Add(_ok);
            foreach (var q in _questionsC)
                Controls.Add(q);
            foreach (var q in _questionsW)
                Controls.Add(q);
            Controls.Add(_head);
        }
        /// <summary>
        /// функция, задающая параметры по умолчанию
        /// </summary>
        /// <param name="numberquestions">число Select вопросов</param>
        private void InitComponents(int numberquestions)
        {
            _questions = new List<SelectQuestion>();
            for (var i = 0; i < numberquestions - 2; i++)
                _questions.Add(new SelectQuestion(i));
            _questionsC = new List<QuestionCheck>();
            _questionsC.Add(new QuestionCheck(numberquestions - 2));
            _questionsW = new List<InputQuestion>();
            _questionsW.Add(new InputQuestion(numberquestions - 1));

            //с кнопкой
            _ok = new Button
            {
                Text = "ОК",
                Location = new Point(10, 10 + _questionsW[_questionsW.Count - 1].Location.Y + _questionsW[_questionsW.Count - 1].Size.Height)
            };
            _ok.Click += OkClick;
            //с заголовком
            _head = new Label { Location = new Point(100, 10), Size = new Size(100, 30), Text = "Тест по Истории" };
            //с временной кнопкой
        }
        /// <summary>
        /// функция, задающая элементам значения из xml файла
        /// </summary>
        /// <param name="fileName">имя xml файла</param>
        private void ReadFromFile(string fileName)
        {
            QuestionsInfo xmlTest;
            using (var stream = new StreamReader(fileName))
            {
                var reader = new XmlSerializer(typeof(QuestionsInfo));
                xmlTest = reader.Deserialize(stream) as QuestionsInfo;
            }
            //размеры вопрососв
            Ssize = new List<int>();
            Csize = new List<int>();
            Wsize = new List<int>();
            // ReSharper disable once PossibleNullReferenceException
            _questions = new List<SelectQuestion>(xmlTest.QuestionsS.Count);
            for (var i = 0; i < _questions.Capacity; i++)
                _questions.Add(new SelectQuestion(i + 1, xmlTest.QuestionsS[i]));
            //Check вопросы
            _questionsC = new List<QuestionCheck>(xmlTest.QuestionsC.Count);
            for (var i = 0; i < _questionsC.Capacity; i++)
                _questionsC.Add(new QuestionCheck(i + 1, xmlTest.QuestionsC[i]));
            //Input вопросы
            _questionsW = new List<InputQuestion>(xmlTest.QuestionsW.Count);
            for (var i = 0; i < _questionsW.Capacity; i++)
                _questionsW.Add(new InputQuestion(i + 1, xmlTest.QuestionsW[i]));
            //заголовок
            _head = new Label()
            {
                Location = new Point(100, 10),
                Size = new Size(400, 30),
                Text = xmlTest.Text
            };
            //с формой
            Text = xmlTest.Text;
            //с кнопкой
            _ok = new Button
            {
                Text = "ОК",
                Location = new Point(10, 10 + _questionsW[_questionsW.Count-1].Location.Y + _questionsW[_questionsW.Count - 1].Size.Height)
            };
            _ok.Click += OkClick;
        }
        #endregion
        //-----
        #region functions
        #region getset
        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }
        public override sealed bool AutoScroll
        {
            get { return base.AutoScroll; }
            set { base.AutoScroll = value; }
        }
        #endregion
        /// <summary>
        /// обработчик события для кнопки ОК
        /// проверяет тест
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkClick(Object sender, EventArgs e)
        {
            ProgramTest.Open = !ProgramTest.Open;
            _points = 0;
            foreach (var q in _questions)
            {
                _points += q.Check();
            }
            foreach (var q in _questionsC)
            {
                _points += q.Check();
            }
            foreach (var q in _questionsW)
            {
                _points += q.Check();
            }
            ProgramTest.Points = _points;
            Close();
        }
        /// <summary>
        /// функция, вписывающая во все вопросы вправильные ответы
        /// </summary>
        private void Cheating(int passnumber)
        {
            switch
            (passnumber)
            {
                case 0:
                    foreach (var q in _questions)
                        q.Cheat();
                    foreach (var q in _questionsC)
                        q.Cheat();
                    foreach (var q in _questionsW)
                        q.Cheat();
                    break;
                case 1:
                    if (!_cheated)
                    {
                        _wrong = R.Next(_questions.Count);
                        _cheated = !_cheated;
                    }
                    for (var i = 0; i < _questions.Count; i++)
                        if (i != _wrong)
                            _questions[i].Cheat();
                    for (var i = 0; i < _questionsC.Count; i++)
                        if (i != _wrong)
                            _questionsC[i].Cheat();
                    for (var i = 0; i < _questionsW.Count; i++)
                        if (i != _wrong)
                            _questionsW[i].Cheat();
                    break;
                case 2:
                    if (!_cheated)
                    {
                        _cheated = !_cheated;
                        _wList = new List<int>(4);
                        for (int i = 0; i < _wList.Capacity; i++)
                        {
                            bool a = true, b = true;
                            while (b)
                            {
                                b = false;
                                if (a)
                                    _wList.Add(0);
                                a = false;
                                _wList[i] = R.Next(_questions.Count);
                                for (int j = i; j > 0; j--)
                                {
                                    if (_wList[i] == _wList[i - j])
                                        b = true;
                                }
                            }
                        }
                    }
                    for (var i = 0; i < _questions.Count; i++)
                    {
                        var b = true;
                        foreach (var w in _wList)
                            if (i == w)
                                b = false;
                        if (b)
                            _questions[i].Cheat();
                    }
                    break;
            }

        }
        /// <summary>
        /// обработчик события, вызывающий ф-цию Cheating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cheat(object sender, KeyPressEventArgs e)
        {
            if (_lettercount == 0)
            {
                for (int i = 0; i < _pass.Length; i++)
                    if (e.KeyChar == _pass[i][0])
                        _passcount = i;
            }
            if (e.KeyChar == _pass[_passcount][_lettercount])
            {
                if (_lettercount == _pass[_passcount].Length - 1)
                {
                    Cheating(_passcount);
                    _lettercount = 0;
                }
                else
                    _lettercount++;
            }
            else
                _lettercount = 0;
        }
        /// <summary>
        /// функция, разрешающая скроллить окно
        /// (это делается в отдельной функции из-за того, что так скролл остатся сверху)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Scrolling(object sender, EventArgs e)
        {
            AutoScroll = true;
            AutoScrollPosition = new Point(0, 0);
        }
        #endregion
    }
}