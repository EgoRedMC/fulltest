﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FullTest
{
    class ChooseYourTest : Form
    {
        #region fields
        /// <summary>
        /// меню
        /// </summary>
        private MenuStrip _menu;
        /// <summary>
        /// менюитем первого уровня ("Варианты")
        /// </summary>
        private ToolStripMenuItem _choose;
        /// <summary>
        /// менюитем второго уровня ("последний")
        /// </summary>
        private ToolStripMenuItem _last;
        /// <summary>
        /// массив менюитемов третьего уровня (буквы классов)
        /// </summary>
        private ToolStripMenuItem[][] _classes;
        /// <summary>
        /// массив менюитемов второго уровня (номера классов)
        /// </summary>
        private ToolStripMenuItem[] _clas;
        /// <summary>
        /// кнопка "выбрать"
        /// </summary>
        private Button _ok;
        /// <summary>
        /// лейбл, в котором высвечивается выбранный класс
        /// </summary>
        private Label _text;
        /// <summary>
        /// имя выбранного файла
        /// </summary>
        private string _chosen;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public ChooseYourTest()
        {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            InitElements();
            AddToControls();
        }
        /// <summary>
        /// функция, задающая параметры всем элементам
        /// </summary>
        private void InitElements()
        {

            //
            _menu = new MenuStrip();
            _choose = new ToolStripMenuItem();
            _menu.Items.Add(_choose);
            _choose.Text = "Варианты";
            //
            _last = new ToolStripMenuItem { Text = "Последний" };
            _last.Click += LastClick;
            _choose.DropDownItems.Add(_last);
            //
            _classes = new ToolStripMenuItem[7][];
            _clas = new ToolStripMenuItem[7];
            for (int i = 0; i < _classes.Length; i++)
            {
                _classes[i] = new ToolStripMenuItem[3];
                _clas[i] = new ToolStripMenuItem { Text = (i + 5).ToString() };
                _choose.DropDownItems.Add(_clas[i]);
                for (var j = 0; j < _classes[i].Length; j++)
                {
                    _classes[i][j] = new ToolStripMenuItem();
                    switch (j)
                    {
                        case 0:
                            _classes[i][j].Text = "а";
                            break;
                        case 1:
                            _classes[i][j].Text = "б";
                            break;
                        case 2:
                            _classes[i][j].Text = "в";
                            break;
                    }
                    _clas[i].DropDownItems.Add(_classes[i][j]);
                }
            }
            //
            _ok = new Button { Size = new Size(100, 50), Text = "ОК", Location = new Point(90, 100) };
            //
            _text = new Label { Size = new Size(100, 70), Location = new Point(90, 50), Text = "выберите класс" };
            //
            #region eventhandlers
            _classes[0][0].Click += A5;
            _classes[0][1].Click += B5;
            _classes[0][2].Click += C5;
            _classes[1][0].Click += A6;
            _classes[1][1].Click += B6;
            _classes[1][2].Click += C6;
            _classes[2][0].Click += A7;
            _classes[2][1].Click += B7;
            _classes[2][2].Click += C7;
            _classes[3][0].Click += A8;
            _classes[3][1].Click += B8;
            _classes[3][2].Click += C8;
            _classes[4][0].Click += A9;
            _classes[4][1].Click += B9;
            _classes[4][2].Click += C9;
            _classes[5][0].Click += A10;
            _classes[5][1].Click += B10;
            _classes[5][2].Click += C10;
            _classes[6][0].Click += A11;
            _classes[6][1].Click += B11;
            _classes[6][2].Click += C11;
            #endregion
            _ok.Click += OkClick;
        }
        /// <summary>
        /// функция, добавляющая элементы на форму
        /// </summary>
        private void AddToControls()
        {
            Controls.Add(_menu);
            Controls.Add(_ok);
            Controls.Add(_text);
        }
        #endregion
        //-----
        #region functions
        #region eventhandlers for variants
        /*
         * здесь находится большое число обработчиков событий (22, если не ошибаюсь)
         * все они обрабатывают события менюитемов второго и третьих уровней
         */
        private void LastClick(Object sender, EventArgs e)
        {
            _chosen = "последний";
            _text.Text = _chosen;
        }
        private void A5(Object sender, EventArgs e)
        {
            _chosen = "5а";
            _text.Text = _chosen + " класс";
        }
        private void B5(Object sender, EventArgs e)
        {
            _chosen = "5б";
            _text.Text = _chosen + " класс";
        }
        private void C5(Object sender, EventArgs e)
        {
            _chosen = "5в";
            _text.Text = _chosen + " класс";
        }
        private void A6(Object sender, EventArgs e)
        {
            _chosen = "6а";
            _text.Text = _chosen + " класс";
        }
        private void B6(Object sender, EventArgs e)
        {
            _chosen = "6б";
            _text.Text = _chosen + " класс";
        }
        private void C6(Object sender, EventArgs e)
        {
            _chosen = "6в";
            _text.Text = _chosen + " класс";
        }
        private void A7(Object sender, EventArgs e)
        {
            _chosen = "7а";
            _text.Text = _chosen + " класс";
        }
        private void B7(Object sender, EventArgs e)
        {
            _chosen = "7б";
            _text.Text = _chosen + " класс";
        }
        private void C7(Object sender, EventArgs e)
        {
            _chosen = "7в";
            _text.Text = _chosen + " класс";
        }
        private void A8(Object sender, EventArgs e)
        {
            _chosen = "8а";
            _text.Text = _chosen + " класс";
        }
        private void B8(Object sender, EventArgs e)
        {
            _chosen = "8б";
            _text.Text = _chosen + " класс";
        }
        private void C8(Object sender, EventArgs e)
        {
            _chosen = "8в";
            _text.Text = _chosen + " класс";
        }
        private void A9(Object sender, EventArgs e)
        {
            _chosen = "9а";
            _text.Text = _chosen + " класс";
        }
        private void B9(Object sender, EventArgs e)
        {
            _chosen = "9б";
            _text.Text = _chosen + " класс";
        }
        private void C9(Object sender, EventArgs e)
        {
            _chosen = "9в";
            _text.Text = _chosen + " класс";
        }
        private void A10(Object sender, EventArgs e)
        {
            _chosen = "10а";
            _text.Text = _chosen + " класс";
        }
        private void B10(Object sender, EventArgs e)
        {
            _chosen = "10б";
            _text.Text = _chosen + " класс";
        }
        private void C10(Object sender, EventArgs e)
        {
            _chosen = "10в";
            _text.Text = _chosen + " класс";
        }
        private void A11(Object sender, EventArgs e)
        {
            _chosen = "11а";
            _text.Text = _chosen + " класс";
        }
        private void B11(Object sender, EventArgs e)
        {
            _chosen = "11б";
            _text.Text = _chosen + " класс";
        }
        private void C11(Object sender, EventArgs e)
        {
            _chosen = "11в";
            _text.Text = _chosen + " класс";
        }
        #endregion
        /// <summary>
        /// обработчик события для кнопки ОК
        /// сохраняет имя и передаёт его в следующую форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkClick(Object sender, EventArgs e)
        {
            switch (Program.p)
            {
                case 0:
                    if (_text.Text != "выберите класс")
                    {
                        ProgramTest.Open = true;
                        ProgramTest.Name = _chosen;
                        Close();
                    }
                    else
                    {
                        _chosen = "последний";
                        _text.Text = _chosen;
                    }

                    break;
                case 1:
                    if (_text.Text != "выберите класс")
                    {
                        ProgramEdit.Open = true;
                        ProgramEdit.Name = _chosen;
                        Close();
                    }
                    else
                    {
                        _chosen = "последний";
                        _text.Text = _chosen;
                    }
                    break;
            }
        }
        #endregion
    }
}
