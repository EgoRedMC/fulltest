﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace FullTest
{
    public class InstalizingForm : Form
    {
        #region fields
        private readonly Label _loadText;
        private readonly Label _ready;
        private readonly Button _end;
        private readonly Button _start;
        #endregion
        //-----
        public InstalizingForm()
        {
            Size = new Size(450, 230);
            Text = "Установка";
            //работа с лейблом загрузки
            _loadText = new Label
            {
                Size = new Size(150, 50),
                Location = new Point(155, 75),
                Text = "Идёт установка файлов...",
                Visible = false
            };
            Controls.Add(_loadText);
            //работа с кнопкой начала
            _start = new Button {Text = "Начать установку", Location = new Point(140, 60), Size = new Size(150, 50)};
            _start.Click += DoFiles;
            Controls.Add(_start);
            //работа с кнопкой окончания
            _end = new Button
            {
                Text = "Завершить",
                Location = new Point(140, 70),
                Size = new Size(150, 50),
                Visible = false
            };
            _end.Click += Close;
            Controls.Add(_end);
            //работа с лейблом оканчания
            _ready = new Label
            {
                Text = "Установка завершена",
                Size = new Size(150, 20),
                Location = new Point(140, 50),
                Visible = false,
                TextAlign = ContentAlignment.MiddleCenter
            };
            Controls.Add(_ready);
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        //-----
        #region functions
        /// <summary>
        /// eventHandler для кнопки окончания
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close(Object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// eventHandler для нажатия на кнопку установки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoFiles(Object sender, EventArgs e)
        {
            Install();
        }
        /// <summary>
        /// функция начала установки
        /// </summary>
        private void StartInstall()
        {
            _start.Visible = false;
            _loadText.Visible = true;
        }
        /// <summary>
        /// непосредственно, сама функция установки со входящими в неё функциями начала и конца установки
        /// </summary>
        private void Install()
        {
            //начало установки
            StartInstall();
            //сама установка
            var folder = Path.Combine(Program.Path, "HistoryNovikov");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            string file;
            for (var i = 0; i < 7; i++)
                for (var j = 0; j < 3; j++)
                {
                    string letter = null;
                    var number = (i + 4 + 1).ToString();
                    switch (j)
                    {
                        case 0:
                            letter = "а";
                            break;
                        case 1:
                            letter = "б";
                            break;
                        case 2:
                            letter = "в";
                            break;
                    }
                    QuestionsInfo q = new QuestionsInfo(number + letter);
                    if (!Directory.Exists(folder))
                        Directory.CreateDirectory(folder);
                    file = Path.Combine(folder, number + letter + ".xml");
                    XmlSerializer saver = new XmlSerializer(typeof(QuestionsInfo));
                    using (StreamWriter stream = File.Exists(file) ? new StreamWriter(file) : new StreamWriter(File.Create(file)))
                        saver.Serialize(stream, q);
                }
            file = Path.Combine(folder, "open.tst");
            if (!File.Exists(file))
                File.Create(file);
            file = Path.Combine(folder, "last.tst");
            if (!File.Exists(file))
                File.Create(file);
            //после установки
            EndInstall();
        }
        /// <summary>
        /// Функция, сообщающая пользователю о том, что всё готово
        /// </summary>
        private void EndInstall()
        {
            _loadText.Visible = false;
            _end.Visible = true;
            _ready.Visible = true;
        }
        #endregion
    }
}
