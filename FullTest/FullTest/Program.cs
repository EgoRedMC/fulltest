﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FullTest
{
    class Program
    {
        /// <summary>
        /// переменная для хранения пути в папку "Документы"
        /// </summary>
        public static readonly string Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        /// <summary>
        /// инт отвечающий за то, какая часть программы работает
        /// </summary>
        public static int p = -1;

        public static int Choosen = -1;
        [STAThread]
        public static void Main()
        {
            while (true)
            {
                Choosen = -1;
                p = -1;
                Application.Run(new ChooseForm());
                switch (Choosen)
                {
                    case 0:
                        p = 0;
                        ProgramTest.Master();
                        break;
                    case 1:
                        p = 1;
                        ProgramEdit.Master();
                        break;
                    case 2:
                        p = 2;
                        ProgramInstall.Master();
                        break;
                    default:
                        Environment.Exit(0);
                        break;
                }
            }

        }
    }
}
