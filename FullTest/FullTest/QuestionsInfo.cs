﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FullTest
{
    [Serializable]
    [XmlRoot("questions")]
    public class QuestionsInfo
    {
        #region fields
        /// <summary>
        /// заголовок
        /// </summary>
        [XmlElement("text")]
        public string Text;

        /// <summary>
        /// массив select вопросов
        /// </summary>
        [XmlArray("questionsSelect")]
        [XmlArrayItem("question")]
        public List<SelectQuestionInfo> QuestionsS;

        /// <summary>
        /// массив check вопросов
        /// </summary>
        [XmlElement("questionsCheck")]
        public List<QuestionCheckInfo> QuestionsC;

        /// <summary>
        /// массив input вопросов
        /// </summary>
        [XmlElement("questionsInput")]
        public List<InputQuestionInfo> QuestionsW;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public QuestionsInfo()
        {
        }
        /// <summary>
        /// конструктор от класса
        /// </summary>
        public QuestionsInfo(string classname)
        {
            Text = "Тест по истории для " + classname + " класса";
            QuestionsS = new List<SelectQuestionInfo>(8);
            for (int i = 0; i < QuestionsS.Capacity; i++)
                QuestionsS.Add(new SelectQuestionInfo(0));
            QuestionsC = new List<QuestionCheckInfo>(1);
            for (int i = 0; i < QuestionsC.Capacity; i++)
                QuestionsC.Add(new QuestionCheckInfo(0));
            QuestionsW = new List<InputQuestionInfo>(1);
            for (int i = 0; i < QuestionsW.Capacity; i++)
                QuestionsW.Add(new InputQuestionInfo(0));

        }
        #endregion
    }

    //----------
    [Serializable]
    [XmlRoot("quesionSelect")]
    public class SelectQuestionInfo
    {
        #region fields
        /// <summary>
        /// вопрос
        /// </summary>
        [XmlElement("questionText")]
        public string Text;
        /// <summary>
        /// номер правильного ответа
        /// </summary>
        [XmlElement("correctAnswer")]
        public int CorrectAnswer;
        /// <summary>
        /// массив ответов
        /// </summary>
        [XmlArray("answers")]
        [XmlArrayItem("answer")]
        public List<string> Answers;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public SelectQuestionInfo()
        {
        }
        /// <summary>
        /// конструктор для записи
        /// </summary>
        /// <param name="a"></param>
        public SelectQuestionInfo(int a)
        {
            Text = "Вопрос";
            CorrectAnswer = 0;
            Answers = new List<string> { "Ответ 1", "Ответ 2", "Ответ 3", "Ответ 4" };
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// функция, выводящая всю инормацтю в консоль
        /// </summary>
        public void Print()
        {
            Console.WriteLine(Text);
            Console.WriteLine("\t{0}", CorrectAnswer);
            foreach (var answ in Answers)
                Console.WriteLine("\t{0}", answ);
        }
        #endregion
    }

    //----------
    [Serializable]
    [XmlRoot("questionCheck")]
    public class QuestionCheckInfo
    {
        #region fields
        /// <summary>
        /// вопрос
        /// </summary>
        [XmlElement("QuestionText")]
        public string Text;
        /// <summary>
        /// массив ответов
        /// </summary>
        [XmlArray("answers")]
        [XmlArrayItem("answer")]
        public List<string> Answers;
        /// <summary>
        /// массив правильных ответов
        /// </summary>
        [XmlArray("rightanswers")]
        [XmlArrayItem("answer")]
        public List<int> Rightanswers;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public QuestionCheckInfo()
        {
        }
        /// <summary>
        /// конструктор для записи
        /// </summary>
        /// <param name="a"></param>
        public QuestionCheckInfo(int a)
        {
            Text = "Вопрос";
            Answers = new List<string> { "Ответ 1", "Ответ 2", "Ответ 3", "Ответ 4" };
            Rightanswers = new List<int> { 0, 1 };
        }
    #endregion
    //-----
    #region functions
    /// <summary>
    /// функция для вывода всей информации в консоль
    /// </summary>
    public void Print()
        {
            Console.WriteLine(Text);
            foreach (var str in Answers)
                Console.WriteLine("\t{0}", str);
            Console.WriteLine();
            foreach (var num in Rightanswers)
                Console.WriteLine("\t{0}", num);
        }
        #endregion
    }

    //----------
    [Serializable]
    [XmlRoot("questionInput")]
    public class InputQuestionInfo
    {
        #region fields
        /// <summary>
        /// вопрос
        /// </summary>
        [XmlElement("QuestionText")]
        public string Text;
        /// <summary>
        /// правильный ответ
        /// </summary>
        [XmlElement("Answer")]
        public string Answer;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public InputQuestionInfo()
        {
        }
        /// <summary>
        /// конструктор для записи
        /// </summary>
        /// <param name="a"></param>
        public InputQuestionInfo(int a)
        {
            Text = "Вопрос";
            Answer = "Ответ";
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// функция, выводящая всю информацию в консоль
        /// </summary>
        public void Print()
        {
            Console.WriteLine("{0}\n\t{1}", Text, Answer);
        }
        #endregion
    }
}
