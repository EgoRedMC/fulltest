﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FullTest
{
    class SelectQuestionEditBox : GroupBox
    {
        #region fields
        /// <summary>
        /// номер вопроса
        /// </summary>
        private readonly int _questionNumber;
        /// <summary>
        /// лейбл, в котором написано "Вопрос"
        /// </summary>
        private Label _questionLabel;
        /// <summary>
        /// текстбокс в который вписывают сам вопрос
        /// </summary>
        private readonly TextBox _questionBox;
        /// <summary>
        /// лист лейблов, в которых будет написано "Ответ №*"
        /// </summary>
        private List<Label> _answerLabels;
        /// <summary>
        /// лист текстбоксов, в которых будут написаны ответы
        /// </summary>
        private readonly List<TextBox> _answerBoxs;
        /// <summary>
        /// лейбл, в котором написано "Правильный ответ"
        /// </summary>
        private Label _rightLabel;
        /// <summary>
        /// лист радиобатонов, который показывает, какой ответ правильный
        /// </summary>
        private readonly List<RadioButton> _rightButtons;
        /// <summary>
        /// кнопка, добавляющая ещё один вариант ответа
        /// </summary>
        private Button _oneMore;
        /// <summary>
        /// кнопка, убирающая один вариант ответа
        /// </summary>
        private Button _oneLess;

        public new int Height
        {
            get { return (50 + _answerBoxs.Count*30); }
        }

        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public SelectQuestionEditBox()
        {
        }
        /// <summary>
        /// конструктор для считывания из xml заготовки
        /// </summary>
        /// <param name="number">номер вопроса</param>
        /// <param name="question">xml заготовка в формате SelectQuestionInfo</param>
        public SelectQuestionEditBox(int number, SelectQuestionInfo question)
        {
            //-с тем, что имеет отношение к xml
            //вопрос
            _questionBox = new TextBox() { Text = question.Text };
            //ответы
            _answerBoxs = new List<TextBox>();
            _rightButtons = new List<RadioButton>();
            foreach (string a in question.Answers)
            {
                _answerBoxs.Add(new TextBox() { Text = a });
                _rightButtons.Add(new RadioButton());
            }
            _rightButtons[question.CorrectAnswer].Checked = true;
            //-с тем, что не имеет отношения к xml
            _questionNumber = number;
            int count = EditTest.Ssize.Sum();
            Size = new Size(765, 10 + 20 + 10 + _answerBoxs.Count * (10 + 20) + 10);
            EditTest.Ssize.Add(Size.Height);
            Location = new Point(10, 10 + 20 + 10 + 30 + 10 + 20 + count);
            //создание контролов, не относящихся к xml,
            //задание всему размеров, положений и добавление всего на форму
            LocaleFields();
            AddToControls();
            //
            _oneMore.Click += OneMoreClick;
            _oneLess.Click += OneLessClick;
        }
        /// <summary>
        /// функция, создающая контролы, не относящиеся к xml,
        /// заданющая всему размеры, положения
        /// </summary>
        private void LocaleFields()
        {
            //-вопрос
            //лейбл
            _questionLabel = new Label()
            {
                Text = "Вопрос",
                Location = new Point(10, 15),
                Size = new Size(70, 20),
                BorderStyle = BorderStyle.FixedSingle
            };
            //текстбокс
            _questionBox.Location = new Point(10 + 70, 15);
            _questionBox.Size = new Size(450, 20);
            //-ответы
            _answerLabels = new List<Label>();
            for (var i = 0; i < _answerBoxs.Count; i++)
            {
                //лейблы
                _answerLabels.Add(new Label()
                {
                    Text = "Ответ № " + (i + 1),
                    Location = new Point(10, 15 + 20 + 5 + 10 + i * 30),
                    Size = new Size(70, 20),
                    BorderStyle = BorderStyle.FixedSingle
                });
                //текстбоксы
                _answerBoxs[i].Location = new Point(10 + _answerLabels[i].Size.Width, 15 + 20 + 5 + 10 + i * 30);
                _answerBoxs[i].Size = new Size(450, 20);
            }
            //-чекбоксы
            //лейбл
            _rightLabel = new Label()
            {
                Text = "Правильный ответ",
                Location = new Point(10 + 70 + 450 + 10, 15),
                Size = new Size(105, 20)
            };
            //сами чекбоксы
            for (var i = 0; i < _rightButtons.Count; i++)
            {
                _rightButtons[i].Location = new Point(10 + 70 + 450 + 10, 15 + 20 + 5 + 10 + i * 30);
                _rightButtons[i].Size = new Size(20, 20);
            }
            //-кнопки
            //+1
            _oneMore = new Button()
            {
                Location = new Point(10 + 70 + 450 + 10 + 105 + 10, 15),
                Size = new Size(100, 45),
                Text = "Добавить один вариант ответа"
            };

            //-1
            _oneLess = new Button()
            {
                Location = new Point(10 + 70 + 450 + 10 + 105 + 10, 15 + 45 + 10),
                Size = new Size(100, 45),
                Text = "Убрать последний вариант ответа"
            };
        }
        /// <summary>
        /// функция, добавляющая все контролы на форму
        /// </summary>
        private void AddToControls()
        {
            //вопрос
            Controls.Add(_questionLabel);
            Controls.Add(_questionBox);
            //ответы
            foreach (var a in _answerBoxs)
                Controls.Add(a);
            foreach (var a in _answerLabels)
                Controls.Add(a);
            //чекбоксы
            Controls.Add(_rightLabel);
            foreach (var r in _rightButtons)
                Controls.Add(r);
            //кнопки
            Controls.Add(_oneMore);
            Controls.Add(_oneLess);
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// обработчик события, добавляющий заготовку для ещё одного ответа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OneMoreClick(object sender, EventArgs e)
        {
            Size = new Size(Size.Width, Size.Height + 30);
            //создание
            _answerBoxs.Add(new TextBox()
            {
                Text = "Ответ " + (_answerBoxs.Count + 1),
                Size = _answerBoxs[0].Size,
                Location = new Point(_answerBoxs[0].Location.X, _answerBoxs[_answerBoxs.Count - 1].Location.Y + 30)
            });
            _answerLabels.Add(new Label()
            {
                Text = "Ответ № " + (_answerLabels.Count + 1),
                Size = _answerLabels[0].Size,
                Location = new Point(_answerLabels[0].Location.X, _answerLabels[_answerLabels.Count - 1].Location.Y + 30),
                BorderStyle = BorderStyle.FixedSingle
            });
            _rightButtons.Add(new RadioButton()
            {
                Size = _rightButtons[0].Size,
                Location = new Point(_rightButtons[0].Location.X, _rightButtons[_rightButtons.Count - 1].Location.Y + 30)
            });
            //добавление на форму
            Controls.Add(_answerBoxs[_answerBoxs.Count - 1]);
            Controls.Add(_answerLabels[_answerLabels.Count - 1]);
            Controls.Add(_rightButtons[_rightButtons.Count - 1]);
            ((EditTest)Parent).ReMove();
        }
        /// <summary>
        /// обработчик события, убирающий последнюю заготовку для ответа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OneLessClick(object sender, EventArgs e)
        {
            var num = _answerBoxs.Count - 1;
            if (num <= 2) return;
            Size = new Size(Size.Width, Size.Height - 30);
            EditTest.Ssize[_questionNumber - 1] -= 30;
            //удаление с формы
            Controls.Remove(_answerBoxs[num]);
            Controls.Remove(_answerLabels[num]);
            Controls.Remove(_rightButtons[num]);
            //удаление
            _answerBoxs.RemoveAt(num);
            _answerLabels.RemoveAt(num);
            _rightButtons.RemoveAt(num);
            ((EditTest)Parent).ReMove();
        }
        #region forxml
        /// <summary>
        /// функция для определения правильного ответа
        /// </summary>
        /// <returns>номер правильного ответа</returns>
        public int GetRightAnswer()
        {
            for (int i = 0; i < _answerBoxs.Count; i++)
                if (_rightButtons[i].Checked)
                    return i;
            return 0;
        }
        /// <summary>
        /// функция для определения текста вопроса
        /// </summary>
        /// <returns></returns>
        public string GetQuestionText()
        {
            return _questionBox.Text;
        }
        /// <summary>
        ///  функция для определения текста ответов
        /// </summary>
        /// <returns></returns>
        public List<string> GetAnswers()
        {
            var returned = new List<string>();
            foreach (var a in _answerBoxs)
                returned.Add(a.Text);
            return returned;
        } 
        #endregion
        #endregion
    }
}
