﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FullTest
{
    class SaveDialog : Form
    {
        #region fields
        private int _clicked;
        #endregion

        #region constructors

        public SaveDialog()
        {
            Text = "Сохранить?";
            Size = new Size(30 + 70 + 30 + 70 + 50 + 70 + 30, 200);
            Label l = new Label()
            {
                Text = "Сохранить файл?",
                Location = new Point(30 + 70 + 30, 30)
            };
            Button y = new Button()
            {
                Text = "Да",
                Size = new Size(70, 30),
                Location = new Point(30, 30 + 20 + 30)
            };
            y.Click += Yes;
            Button n = new Button()
            {
                Text = "Нет",
                Size = new Size(70, 30),
                Location = new Point(30 + 70 + 30, 30 + 20 + 30)
            };
            n.Click += No;
            Button c = new Button()
            {
                Text = "Отмена",
                Size = new Size(70, 30),
                Location = new Point(30 + 70 + 30 + 70 + 30, 30 + 20 + 30)
            };
            c.Click += Cancel;
            Controls.Add(y);
            Controls.Add(n);
            Controls.Add(l);
            Controls.Add(c);
        }

        #endregion
        //
        #region functions

        private void Yes(object sender,EventArgs e)
        {
            _clicked = 0;
            this.Close();
        }
        private void No(object sender, EventArgs e)
        {
            _clicked = 1;
            this.Close();
        }
        private void Cancel(object sender, EventArgs e)
        {
            _clicked = 2;
            this.Close();
        }

        /// <summary>
        /// тут вообще магия происходит, хз как это описать
        /// </summary>
        /// <returns></returns>
        public int ShowAsDialog()
        {
            ShowDialog();
            return _clicked;
        }
        #endregion
    }
}
