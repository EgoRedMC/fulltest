﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Xml.Serialization;

namespace FullTest
{
    class EditTest : Form
    {
        #region fields
        /// <summary>
        ///кнопка ОК (принятие)
        /// </summary>
        private Button _save;
        /// <summary>
        /// лист групбоксов для редактирования select вопросов
        /// (1 групбокс = 1 вопрос)
        /// </summary>
        private List<SelectQuestionEditBox> _questions;
        /// <summary>
        /// групбокс для редактирования check вопроса
        /// </summary>
        private List<QuestionChecknEditBox> _questionsC;
        /// <summary>
        /// групбокс для редактирования input вопроса
        /// </summary>
        private List<InputQuestionEditBox> _questionsW;
        /// <summary>
        /// лейбл, в котором написано "Заголовок
        /// </summary>
        private Label _headLabel;
        /// <summary>
        /// текстбокс в котором написан заголовок
        /// </summary>
        private TextBox _headBox;
        /// <summary>
        /// заголовок редактора
        /// </summary>
        private Label _head;
        /// <summary>
        /// номер класса
        /// </summary>
        private Label _class;
        #region sizes
        /// <summary>
        /// лист размеров SelectQuestion по OY
        /// </summary>
        public static List<int> Ssize;
        /// <summary>
        /// лист размеров QuestionCheck по OY
        /// </summary>
        public static List<int> Csize;
        /// <summary>
        /// лист размеров InputQuestion по OY
        /// </summary>
        public static List<int> Wsize;
        #endregion
        /// <summary>
        /// имя файла, забиваемого в readfromfile() и saving()
        /// </summary>
        private readonly string _file;
        /// <summary>
        /// xml заготовка формата QuestionsInfo
        /// </summary>
        private QuestionsInfo _xmlTest;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public EditTest()
        {
        }
        /// <summary>
        /// конструктор формы для считывания из xml файла
        /// </summary>
        /// <param name="fileName">имя файла, который перенаправит на xml файл</param>
        public EditTest(string fileName)
        {
            try
            {
                //перенаправление на основной файл
                var stream = new StreamReader(fileName);
                _file = stream.ReadLine();
                stream.Close();
                //работа с формой
                Ssize = new List<int>();
                Wsize = new List<int>();
                Csize = new List<int>();
                Size = new Size(810, 640);
                HScroll = false;
                AcceptButton = _save;
                FormBorderStyle = FormBorderStyle.FixedSingle;
                MaximizeBox = false;
                //задание элементам параметров
                ReadFromFile(_file);
                AddToControls();
                //
                Shown += Scrolling;
                _save.Click += Saving;
                Closing += CheckSaving;
            }
            //в случае отсутствия файла
            catch (FileNotFoundException)
            {
                MessageBox.Show("Ошибка загрузки теста из файла. Приложение будет завершено. Убедитесь, что файл данного теста существует.\nВ крайнем случае обратитесь к Егору");
                Environment.Exit(1);
            }
        }
        /// <summary>
        /// функция, добавляющая все контролы на форму
        /// </summary>
        private void AddToControls()
        {
            //кнопка ок
            Controls.Add(_save);
            //редакторы select вопросов
            foreach (var q in _questions)
                Controls.Add(q);
            //редакторы check вопроса
            foreach (var q in _questionsC)
                Controls.Add(q);
            //редакторы input вопроса
            foreach (var q in _questionsW)
                Controls.Add(q);
            //редактор заголовка
            Controls.Add(_headLabel);
            Controls.Add(_headBox);
            //заголовки редактора
            Controls.Add(_head);
            Controls.Add(_class);
        }
        /// <summary>
        /// функция для считывания формы из xml файла
        /// </summary>
        /// <param name="fileName">полное имя xml файла</param>
        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        private void ReadFromFile(string fileName)
        {
            using (var stream = new StreamReader(fileName))
            {
                var reader = new XmlSerializer(typeof(QuestionsInfo));
                _xmlTest = reader.Deserialize(stream) as QuestionsInfo;
            }
            //заголовки редактора
            Text = "Редактор тестов";
            _head = new Label()
            {
                Text = "Редактор тестов",
                Size = new Size(150, 30),
                Location = new Point(this.Size.Width / 2 - 75, 10),
                BorderStyle = BorderStyle.FixedSingle,
                Font = new Font(new FontFamily("Times New Roman"), 15)
            };
            _class = new Label()
            {
                Text = String.Format("{0}{1} класс", fileName[fileName.Length - 6], fileName[fileName.Length - 5]),
                Size = new Size(70, 20),
                Location = new Point(this.Size.Width / 2 - 35, 10 + 30 + 10),
                BorderStyle = BorderStyle.FixedSingle
            };
            //редактор заголовка
            _headLabel = new Label()
            {
                Text = "Заголовок",
                Size = new Size(70, 20),
                Location = new Point(10 + 10, 50 + 10 + 20),
                BorderStyle = BorderStyle.FixedSingle
            };
            _headBox = new TextBox()
            {
                Text = _xmlTest.Text,
                Size = new Size(450, 20),
                Location = new Point(10 + 10 + 70, 50 + 10 + 20)
            };
            //редакторы select вопросов
            _questions = new List<SelectQuestionEditBox>();
            for (var i = 0; i < _xmlTest.QuestionsS.Count; i++)
                _questions.Add(new SelectQuestionEditBox(i + 1, _xmlTest.QuestionsS[i]));
            //редакторы check вопроса
            _questionsC = new List<QuestionChecknEditBox>();
            for (var i = 0; i < _xmlTest.QuestionsC.Count; i++)
                _questionsC.Add(new QuestionChecknEditBox(i + 1, _xmlTest.QuestionsC[i]));
            //редакторы input вопроса
            _questionsW = new List<InputQuestionEditBox>();
            for (var i = 0; i < _xmlTest.QuestionsW.Count; i++)
                _questionsW.Add(new InputQuestionEditBox(i + 1, _xmlTest.QuestionsW[i]));
            //с кнопкой
            _save = new Button
            {
                Text = "Сохранить",
                Location = new Point(10, _questionsW[_questionsW.Count-1].Location.Y + _questionsW[_questionsW.Count - 1].Size.Height + 10)
            };
        }
        #endregion
        //-----
        #region functions
        /// <summary>
        /// функция, разрешающая скроллить окно
        /// (это делается в отдельной функции из-за того, что так скролл остатся сверху)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Scrolling(object sender, EventArgs e)
        {
            AutoScroll = true;
            AutoScrollPosition = new Point(0, 0);
        }
        /// <summary>
        /// функция, подгоняющая местоположения editbox ов от их размеров
        /// </summary>
        public void ReMove()
        {
            for (int i = 1; i < _questions.Count; i++)
            {
                _questions[i].Location = new Point(_questions[i - 1].Location.X, _questions[i - 1].Location.Y + _questions[i - 1].Height);
            }
            for (int i = 1; i < _questionsC.Count; i++)
            {
                _questionsC[i].Location = new Point(_questionsC[i - 1].Location.X, _questionsC[i - 1].Location.Y + _questionsC[i - 1].Height);
            }
            for (int i = 1; i < _questionsW.Count; i++)
            {
                _questionsW[i].Location = new Point(_questionsW[i - 1].Location.X, _questionsW[i - 1].Location.Y + _questionsW[i - 1].Height);
            }
            _save.Location = new Point(_questionsW[_questionsW.Count-1].Location.X, _questionsW[_questionsW.Count - 1].Location.Y + _questionsW[_questionsW.Count - 1].Height + 10);
        }
        /// <summary>
        /// функция, сохраняющая все изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Saving(object sender, EventArgs e)
        {
            SaveToBuffer();
            //
            var xmlSaver = new XmlSerializer(typeof(QuestionsInfo));
            using (var stream = new StreamWriter(_file))
            {
                xmlSaver.Serialize(stream, _xmlTest);
            }
        }

        /// <summary>
        /// проверка на то, сохранён ли файл (там жопа вообще, разбираться надо)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckSaving(object sender, CancelEventArgs e)
        {
            QuestionsInfo xmlTemp;
            using (var stream = new StreamReader(_file))
            {
                var reader = new XmlSerializer(typeof(QuestionsInfo));
                xmlTemp = reader.Deserialize(stream) as QuestionsInfo;
            }
            //
            SaveToBuffer();
            //
            if (AreSame(xmlTemp, _xmlTest)) return;
            var q = new SaveDialog();
            switch (q.ShowAsDialog())
            {
                case 0:
                    Saving(sender, e);
                    break;
                case 1:
                    break;
                case 2:
                    e.Cancel = true;
                    break;
            }


        }

        /// <summary>
        /// функция для проверки идеинтичности файлов теста
        /// </summary>
        /// <param name="file">файл сохранёный на диске</param>
        /// <param name="test">файл сохранённый с помощью ф-ции SaveToBuffer</param>
        /// <returns></returns>
        private bool AreSame(QuestionsInfo file, QuestionsInfo test)
        {
            if (file.Text != test.Text) return false;
            //
            if (file.QuestionsS.Count != test.QuestionsS.Count) return false;
            for (int i = 0; i < file.QuestionsS.Count; i++)
            {
                if (file.QuestionsS[i].Text != test.QuestionsS[i].Text) return false;
                if (file.QuestionsS[i].CorrectAnswer != test.QuestionsS[i].CorrectAnswer) return false;
                if (file.QuestionsS[i].Answers.Count != test.QuestionsS[i].Answers.Count) return false;
                for (int j = 0; j < file.QuestionsS[i].Answers.Count; j++)
                {
                    if (file.QuestionsS[i].Answers[j] != test.QuestionsS[i].Answers[j]) return false;
                }

            }
            //
            if (file.QuestionsC.Count != test.QuestionsC.Count) return false;
            for (int i = 0; i < file.QuestionsC.Count; i++)
            {
                if (file.QuestionsC[i].Text != test.QuestionsC[i].Text) return false;
                if (file.QuestionsC[i].Rightanswers.Count != test.QuestionsC[i].Rightanswers.Count) return false;
                if (file.QuestionsC[i].Answers.Count != test.QuestionsC[i].Answers.Count) return false;
                for (int j = 0; i < file.QuestionsC[i].Rightanswers.Count; i++)
                    if (file.QuestionsC[i].Rightanswers[i] != test.QuestionsC[i].Rightanswers[i]) return false;
                for (int j = 0; j < file.QuestionsS[i].Answers.Count; j++)
                {
                    if (file.QuestionsC[i].Answers[j] != test.QuestionsC[i].Answers[j]) return false;
                }

            }
            //
            if (file.QuestionsC.Count != test.QuestionsC.Count) return false;
            for (int i = 0; i < file.QuestionsC.Count; i++)
            {
                if (file.QuestionsW[i].Answer != test.QuestionsW[i].Answer) return false;
            }
            //
            return true;
        }

        /// <summary>
        /// функция для сохранения изменений в виде файла, способного к сравнению
        /// </summary>
        private void SaveToBuffer()
        {
            _xmlTest = new QuestionsInfo
            {
                Text = _headBox.Text,
                QuestionsS = new List<SelectQuestionInfo>(),
                QuestionsC = new List<QuestionCheckInfo>(),
                QuestionsW = new List<InputQuestionInfo>()
                
            };
            foreach (SelectQuestionEditBox sq in _questions)
            {
                _xmlTest.QuestionsS.Add(new SelectQuestionInfo()
                {
                    CorrectAnswer = sq.GetRightAnswer(),
                    Text = sq.GetQuestionText(),
                    Answers = sq.GetAnswers()
                });
            }
            foreach(QuestionChecknEditBox qc in _questionsC)
            {
                _xmlTest.QuestionsC.Add(new QuestionCheckInfo()
                {
                    Answers = qc.GetAnswers(),
                    Text = qc.GetQuestionText(),
                    Rightanswers = qc.GetRightAnswers()
                });
            }
            foreach (InputQuestionEditBox iq in _questionsW)
            {
                _xmlTest.QuestionsW.Add(new InputQuestionInfo()
                {
                    Text = iq.GetQuestionText(),
                    Answer = iq.GetRightAnswer()
                });
            }
        }
        #endregion
    }
}
