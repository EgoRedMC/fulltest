﻿using System;
using System.IO;
using System.Windows.Forms;

namespace FullTest
{
    static class ProgramEdit
    {
        /// <summary>
        /// переменнная, разрешаюшая следующую вторую форму
        /// </summary>
        public static bool Open;
        /// <summary>
        /// имя файла, передающееся во вторую форму
        /// </summary>
        public static string Name;
        /// <summary>
        /// перемнная, хранящая путь к папке "мои документы"
        /// </summary>
        private static readonly string Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\HistoryNovikov\";/// <summary>
        
        ///<summary>
        /// Его величество Main()
        /// </summary>
        [STAThread]
        public static int Master()
        {
            Application.EnableVisualStyles();
            //
            try
            {
                var stream1 = new StreamWriter(@Path + "open.tst");
                stream1.Close();
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Ошибка поиска файла. Убедитесь, что программа установлена правильно.\nПопробуйте провести повторную установку.\nВ крайнем случае обратитесь к Егору");
                Environment.Exit(1);
            }
            //первая форма
            Application.Run(new ChooseYourTest());
            if (!Open) return 0;
            Open = !Open;
            //вторая форма
            try
            {
                if (Name != "последний")
                {
                    var stream = new StreamWriter(@Path + "open.tst");
                    stream.Write(Path + Name + ".xml");
                    stream.Close();
                    var stream1 = new StreamWriter(@Path + "last.tst");
                    stream1.Write(Path + Name + ".xml");
                    stream1.Close();
                    Application.Run(new EditTest(@Path + "open.tst"));
                }
                else
                    using (var stream = new StreamReader(@Path + "last.tst"))
                        if (stream.ReadLine() != null)
                            Application.Run(new EditTest(@Path + "last.tst"));
                        else
                        {
                            MessageBox.Show("Извините, такого файла не существует");
                            Environment.Exit(1);
                        }
            }
            catch (DirectoryNotFoundException)
            {
                MessageBox.Show("Ошибка поиска файла. Убедитесь, что программа установлена правильно.\nПопробуйте провести повторную установку.\nВ крайнем случае обратитесь к Егору");
                Environment.Exit(1);
            }
            return 0;
        }
    }
}
