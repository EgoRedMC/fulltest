﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FullTest
{
    class InputQuestionEditBox : GroupBox
    {
        #region fields
        /// <summary>
        /// номер вопроса
        /// </summary>
        private int _questionNumber;
        /// <summary>
        /// лейбл, в котором написано "Вопрос"
        /// </summary>
        private Label _questionLabel;
        /// <summary>
        /// текстбокс в который вписывают сам вопрос
        /// </summary>
        private TextBox _questionBox;
        /// <summary>
        /// лейбл, в котором будет написано "Вопрос"
        /// </summary>
        private Label _answerLabel;
        /// <summary>
        /// текстбокс в котором будет написан правильный ответ
        /// </summary>
        private TextBox _answerBox;
        /// <summary>
        /// лейбл, в котором пишется "Пишите ответ с большой буквы" или типа того
        /// </summary>
        private Label _help;

        public new int Height
        {
            get { return (10 + 20 + 10 + 20 + 10 + 20 + 10); }
        }
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор по умолчанию
        /// </summary>
        public InputQuestionEditBox()
        {
        }

        public InputQuestionEditBox(int number, InputQuestionInfo question)
        {
            //-с тем, что имеет отношение к xml
            //вопрос
            _questionBox = new TextBox() { Text = question.Text };
            //ответ
            _answerBox = new TextBox() { Text = question.Answer };
            //-с тем, что не имеет отношения к xml
            _questionNumber = number;
            Size = new Size(765, 10 + 20 + 10 + 20 + 10 + 20 + 10);
            int count = EditTest.Ssize.Sum();
            count += EditTest.Csize.Sum()+EditTest.Wsize.Sum();
            Location = new Point(10, 30 + 50 + 20 + count);
            EditTest.Wsize.Add(Size.Height);
            //создание контролов, не относящихся к xml,
            //задание всему размеров, положений и добавление всего на форму
            LocaleFields();
            AddToControls();
        }
        /// <summary>
        /// функция, создающая контролы, не относящиеся к xml,
        /// заданющая всему размеры, положения
        /// </summary>
        private void LocaleFields()
        {
            //-вопрос
            //лейбл
            _questionLabel = new Label()
            {
                Text = "Вопрос",
                Location = new Point(10, 15),
                Size = new Size(70, 20),
                BorderStyle = BorderStyle.FixedSingle
            };
            //текстбокс
            _questionBox.Location = new Point(10 + 70, 15);
            _questionBox.Size = new Size(450, 20);
            //-лейбл help
            _help = new Label()
            {
                Text = "Ответ пишите с большой буквы",
                Size = new Size(70 + 450, 20),
                Location = new Point(10, 15 + 20 + 10),
                BorderStyle = BorderStyle.FixedSingle
            };
            //-ответ
            //лейбл
            _answerLabel = new Label()
            {
                Text = "Ответ",
                Location = new Point(10, 15 + 20 + 10 + 20 + 10),
                Size = new Size(70, 20),
                BorderStyle = BorderStyle.FixedSingle
            };
            //текстбокс
            _answerBox.Location = new Point(10 + 70, 15 + 20 + 10 + 20 + 10);
            _answerBox.Size = new Size(450, 20);
        }
        /// <summary>
        /// функция, добавляющая все контролы на форму
        /// </summary>
        private void AddToControls()
        {
            //вопрос
            Controls.Add(_questionLabel);
            Controls.Add(_questionBox);
            //ответ
            Controls.Add(_answerLabel);
            Controls.Add(_answerBox);
            //лейбл _help
            Controls.Add(_help);
        }
        #endregion
        //-----
        #region functions
        #region forxml
        /// <summary>
        /// функция для определения правильного ответа
        /// </summary>
        /// <returns>текст правильного ответа</returns>
        public string GetRightAnswer()
        {
            return _answerBox.Text;
        }
        /// <summary>
        /// функция для определения текста вопроса
        /// </summary>
        /// <returns></returns>
        public string GetQuestionText()
        {
            return _questionBox.Text;
        }
        #endregion
        #endregion
    }
}
