﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FullTest
{
    public class ChooseForm : Form
    {
        #region fields
        /// <summary>
        /// кнопка, отправляющая на Test
        /// </summary>
        private Button _test;
        /// <summary>
        /// кнопка, отправляющая на InstallTest
        /// </summary>
        private Button _install;
        /// <summary>
        /// кнопка, отправляющая на EditTest
        /// </summary>
        private Button _edit;
        #endregion
        //-----
        #region constructors
        /// <summary>
        /// конструктор
        /// </summary>
        public ChooseForm()
        {
            Size = new Size(275, 290);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Text = "Выберите задачу";
            MaximizeBox = false;
            //
            var textLabel = new Label
            {
                Size = new Size(200, 20),
                Text = "Выберите задачу",
                Font = new Font("Times New Roman", 13),
                TextAlign = ContentAlignment.MiddleCenter,
                Location = new Point(30, 20),
            };
            _test = new Button
            {
                Size = new Size(200, 40),
                Text = "Решить тест",
                Font = new Font("Times New Roman", 15),
                TextAlign = ContentAlignment.MiddleCenter,
                Location = new Point(30, 60),

            };
            _edit = new Button
            {
                Size = new Size(200, 40),
                Text = "Изменить тест",
                Font = new Font("Times New Roman", 15),
                TextAlign = ContentAlignment.MiddleCenter,
                Location = new Point(30, 60 + 40 + 20)
            };
            _install = new Button
            {
                Size = new Size(200, 40),
                Text = "Установить тесты",
                Font = new Font("Times New Roman", 15),
                TextAlign = ContentAlignment.MiddleCenter,
                Location = new Point(30, 60 + 40 + 20 + 40 + 20)
            };
            //
            _install.Click += Install;
            _edit.Click += Edit;
            _test.Click += Test;
            //
            Controls.Add(textLabel);
            Controls.Add(_test);
            Controls.Add(_edit);
            Controls.Add(_install);
        }
        #endregion
        //------
        #region functions
        /// <summary>
        /// обработчики события для обозначения следующей формы
        /// </summary>
        #region Eventhandlers for buttons
        private void Install(object sender, EventArgs e)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var folder = System.IO.Path.Combine(path, "HistoryNovikov");
            Program.Choosen = 2;
            if (Directory.Exists(folder))
            {
                switch (
                    MessageBox.Show(
                        "Такая папка уже существует\nПри нажатии кнопки \"Да\" все тесты будут переписаны по умолчанию",
                        "Вы уверены?", MessageBoxButtons.YesNo))
                {
                    case DialogResult.No:
                        Program.Choosen = -1;
                        break;
                }
            }
            Close();
        }
        private void Test(object sender, EventArgs e)
        {
            Program.Choosen = 0;
            Close();
        }
        private void Edit(object sender, EventArgs e)
        {
            Program.Choosen = 1;
            Close();
        }
        #endregion
        #endregion
    }
}
